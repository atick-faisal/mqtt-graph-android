package ai.atick.mqttgraph;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MqttAndroidClient client;
    private String SERVER_URL = "tcp://192.168.0.104:1883";
    private String TOPIC = "mqtt/value";
    private boolean connectionFlag = false;
    private int value = 0;
    private int counter = 0;
    private boolean notDestroyed = true;
    private LineChart chartL;
    private BarChart chartB;
    private List<Entry> entriesL;
    private List<BarEntry> entriesB;
    private Description description;
    private YAxis yAxisL;
    private XAxis xAxisL;
    private Legend legendL;
    private YAxis yAxisB;
    private XAxis xAxisB;
    private Legend legendB;

    static int DESCRIPTION_TEXT_SIZE = 12;
    private int MAX_DATA_COUNT = 16;
    private int REFRESH_RATE = 100;
    private static int white, green;

    ////////////////////////////////////////////////////////////////////////////
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            addData(value);
            //value = 0;
        }
    };
    ////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ////////////////////////////////////////////////////////////////////////
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            String ip = bundle.getString("ip");
            String dp = bundle.getString("dp");
            String rf = bundle.getString("rf");
            if(ip != null && dp != null && rf != null) {
                SERVER_URL = "tcp://" + ip + ":1883";
                MAX_DATA_COUNT = Integer.parseInt(dp);
                REFRESH_RATE = Integer.parseInt(rf);
                connectToBroker();
            } else {
                connectToBroker();
            }
        } else {
            connectToBroker();
        }
        ////////////////////////////////////////////////////////////////////////

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.hide();
        }

        ////////////////////////////////////////////////////////////////////////

        entriesL = new ArrayList<>();
        entriesB = new ArrayList<>();
        chartL = findViewById(R.id.chartL);
        chartB = findViewById(R.id.chartB);
        ImageView button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        ///////////////////////////////////////////////////////////////////////////
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
        ///////////////////////////////////////////////////////////////////////////
        initChart();
        formatChart();
        timer();

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void initChart() {
        white = getResources().getColor(R.color.white);
        green = getResources().getColor(R.color.green);
        description = new Description();
        yAxisL = chartL.getAxisLeft();
        xAxisL = chartL.getXAxis();
        legendL = chartL.getLegend();

        yAxisB = chartB.getAxisLeft();
        xAxisB = chartB.getXAxis();
        legendB = chartB.getLegend();
        
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void formatChart() {
        description.setText("Maximum Voltage = 0mV");
        description.setTextColor(white);
        description.setTextSize(DESCRIPTION_TEXT_SIZE);

        chartL.setDescription(description);
        yAxisL.setTextColor(white);
        xAxisL.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisL.setTextColor(white);
        legendL.setEnabled(true);
        legendL.setTextColor(white);
        legendL.setForm(Legend.LegendForm.CIRCLE);

        chartB.setDescription(description);
        yAxisB.setTextColor(white);
        xAxisB.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisB.setTextColor(white);
        legendB.setEnabled(true);
        legendB.setTextColor(white);
        legendB.setForm(Legend.LegendForm.CIRCLE);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void addData(int value) {
        entriesL.add(new Entry(counter++, (float) value));
        LineDataSet dataSetL = new LineDataSet(entriesL, "Voltage");
        if(dataSetL.getEntryCount() > MAX_DATA_COUNT) {
            dataSetL.removeFirst();
        }
        dataSetL.setCircleColor(green);
        dataSetL.setCircleColorHole(green);
        dataSetL.setColor(green);
        LineData lineData = new LineData(dataSetL);
        description.setText("Maximum Voltage = " + yAxisL.getAxisMaximum() + "mV");
        chartL.setData(lineData);
        chartL.invalidate();

        entriesB.add(new BarEntry(counter++, (float) value));
        BarDataSet dataSetB = new BarDataSet(entriesB, "Voltage");
        if(dataSetB.getEntryCount() > MAX_DATA_COUNT) {
            dataSetB.removeFirst();
        }
        dataSetB.setColor(green);
        BarData barData = new BarData(dataSetB);
        description.setText("Maximum Voltage = " + yAxisB.getAxisMaximum() + "mV");
        chartB.setData(barData);
        chartB.invalidate();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    void connectToBroker() {
        String clientId = MqttClient.generateClientId();
        client = new MqttAndroidClient(this.getApplicationContext(), SERVER_URL, clientId);

        try {
            IMqttToken token = client.connect();
            token.setActionCallback(new IMqttActionListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Toast.makeText(getApplicationContext(), "Connected to : " + SERVER_URL, Toast.LENGTH_SHORT).show();
                    connectionFlag = true;
                    subscribeToTopic(TOPIC);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public void subscribeToTopic(String topic) {
        try {
            if (client.isConnected()) {
                client.subscribe(topic, 0);
                Toast.makeText(getApplicationContext(), "Subscribed", Toast.LENGTH_SHORT).show();
                client.setCallback(new MqttCallback() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void connectionLost(Throwable cause) {
                        Toast.makeText(getApplicationContext(), "Connection Lost", Toast.LENGTH_SHORT).show();
                        connectionFlag = false;
                    }

                    @Override
                    public void messageArrived(String topic, MqttMessage message) {
                        String m = message.toString();
                        value = Integer.parseInt(m);
                    }

                    @Override
                    public void deliveryComplete(IMqttDeliveryToken token) {
                    }
                });
            }
        } catch (Exception ignored) {
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onDestroy() {
        super.onDestroy();
        notDestroyed = false;
        if (connectionFlag) {
            try {
                IMqttToken disconnectToken = client.disconnect();
                disconnectToken.setActionCallback(new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        finish();
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        finish();
                    }
                });
            } catch (MqttException e) {
                e.printStackTrace();
            }
            connectionFlag = false;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////


    private void timer() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (notDestroyed) {
                        long startTime = System.currentTimeMillis();
                        long futureTime = startTime + REFRESH_RATE;
                        while (futureTime > System.currentTimeMillis()) ;
                        handler.sendEmptyMessage(0);

                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
}
