package ai.atick.mqttgraph;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SettingsActivity extends AppCompatActivity {

    EditText ipAddress, maxDataPoints, refreshRate;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ////////////////////////////////////////////////////////////
        ipAddress = findViewById(R.id.ip_address);
        maxDataPoints = findViewById(R.id.max_data_point);
        refreshRate = findViewById(R.id.refresh_rate);
        saveButton = findViewById(R.id.save_button);
        ////////////////////////////////////////////////////////////

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                intent.putExtra("ip", ipAddress.getText().toString());
                intent.putExtra("dp", maxDataPoints.getText().toString());
                intent.putExtra("rf", refreshRate.getText().toString());
                startActivity(intent);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Settings");
            actionBar.setElevation(0);
        }
    }
}
